#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

import ticketing.app

app = ticketing.app.create_app()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True, threaded=True)

# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)
 
from flask_sqlalchemy import orm
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
orm.configure_mappers()


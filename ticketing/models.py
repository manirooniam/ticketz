# -*- coding: utf-8 -*-

from datetime import datetime

import sqlalchemy
from ext import db

class User(db.Model):
    __tablename__ = 'users'
    user_id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.String(36), unique=True, nullable=False)
    email = db.Column(db.String(200), unique=True, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


class Venue(db.Model):
    __tablename__ = 'venues'
    __table_indices__ = (
        db.UniqueConstraint(
            'name',
            'location',
            name='name_location_idx'
   ),)

    venue_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    location = db.Column(db.String(100), unique=True, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


class Event(db.Model):
    __tablename__ = 'events'
    event_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


class Ticket(db.Model):
    __tablename__ = 'tickets'
    ticket_id = db.Column(db.Integer, primary_key=True)
    venue_id = db.Column(
        db.Integer, 
        db.ForeignKey("venues.venue_id")
    )
    event_id = db.Column(
        db.Integer, 
        db.ForeignKey("events.event_id")
    )
    user_id = db.Column(
        db.Integer, 
        db.ForeignKey("users.user_id")
    )

    tid = db.Column(db.String(36), unique=True, nullable=False)
    seat = db.Column(db.Integer, nullable=False)
    isle = db.Column(db.Integer, nullable=False)
    barcode = db.Column(db.Binary, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    user = sqlalchemy.orm.relationship('User', backref='user')
    venue = sqlalchemy.orm.relationship('Venue', backref='venue')
    event = sqlalchemy.orm.relationship('Event', backref='event')


    @property
    def json(self):
        return {
            'ticket': self.tid,
            'seat': self.seat,
            'isle': self.isle,
            'user': self.user.email,
            'venue': self.venue.name,
            'event': self.event.name,
            'date': self.event.date.isoformat()
        }

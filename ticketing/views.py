# -*- coding: utf-8 -*-

import json
import uuid

from flask import Blueprint
from flask import Response 
from flask import request
from sqlalchemy import exc

from ticketing import ext
from ticketing import models

ticket = Blueprint('ticket', __name__)


def response(data=None, status=200):
    if data is None:
        data = []
    return Response(json.dumps(data), status=status, mimetype='application/json')


@ticket.route('/', methods=['POST'], endpoint='index')
def index():
    return response() 


@ticket.route('/api/tickets/<string:tid>', methods=['GET'], endpoint='get_ticket')
def get(tid):
    ticket = models.Ticket.query.filter_by(tid=tid).first()
    if ticket is None:
        return  response(status=404) 

    return response(ticket.json)
 


@ticket.route('/api/tickets', methods=['POST'], endpoint='create_ticket')
def create():
    if request.json == {}:
        return '', 403

    for field in ['user','venue','event', 'seat','isle']:
        if field not in request.json:
            return response, 403


    ticket = models.Ticket(
        user_id = request.json['user'],
        venue_id = request.json['venue'],
        event_id = request.json['event'],
        seat = request.json['seat'],
        isle = request.json['isle'],
        barcode = '\x00',
        tid = str(uuid.uuid4())
    )

    try:
        ext.db.session.add(ticket)
        ext.db.session.commit()
    except exc.IntegrityError:
        return response, 403

    return response(ticket.json)
            

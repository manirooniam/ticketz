# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)

import uuid
from datetime import datetime

from sqlalchemy import exc
from flask_sqlalchemy import SQLAlchemy as sqlalchemy
from flask_sqlalchemy import orm
from flask_testing import TestCase
from werkzeug import datastructures


from ticketing import app
from ticketing import ext
from ticketing import models

class TestModels(TestCase):
    def create_app(self):
        self.app = app.create_app()
        self.app.config['TESTING'] = True
        return self.app

    def create_flask_fixtures(self):
        self._flask_session_fixture = datastructures.CallbackDict()
        self._flask_session_fixture.sid = str(uuid.uuid4())

    def get_engine(self):
        with self.app.test_request_context():
            return ext.db.engine


    def setUp(self):
        super(TestModels, self).setUp()
        orm.configure_mappers()
        ext.db.create_all()
        self._flask_session_fixture = datastructures.CallbackDict()
        self._flask_session_fixture.sid = str(uuid.uuid4())

    
    def tearDown(self):
        super(TestModels, self).tearDown()
        ext.db.session.remove()
        ext.db.drop_all()

    def test_create_user(self):
        uid = str(uuid.uuid4())
        email = 'some@where.com'
        u = models.User(uid=uid, email=email)
        ext.db.session.add(u)
        ext.db.session.commit()
        self.assertEqual(u.user_id, 1)
        self.assertEqual(u.uid, uid)
        self.assertEqual(u.email, email)


    def test_create_duplicate_user(self):
        uid = str(uuid.uuid4())
        email = 'some@where.com'
        u = models.User(uid=uid, email=email)
        ext.db.session.add(u)
        ext.db.session.commit()

        u2 = models.User(uid=uid, email=email)
        ext.db.session.add(u2)
        self.assertRaises(exc.IntegrityError, ext.db.session.commit)


    def test_create_duplicate_email(self):
        uid = str(uuid.uuid4())
        email = 'some@where.com'
        u = models.User(uid=uid, email=email)
        ext.db.session.add(u)
        ext.db.session.commit()

        uid = str(uuid.uuid4())
        email = 'some@where.com'
        u2 = models.User(uid=uid, email=email)
        ext.db.session.add(u2)
        self.assertRaises(exc.IntegrityError, ext.db.session.commit)


    def test_create_venue(self):
        venue = models.Venue(location="Los Angeles", name="magical venue")
        ext.db.session.add(venue)
        ext.db.session.commit()
        self.assertEqual(venue.venue_id, 1)


    def test_create_duplicate_venue(self):
        venue = models.Venue(location="Los Angeles", name="magical venue")
        ext.db.session.add(venue)
        ext.db.session.commit()

        venue2 = models.Venue(location="Los Angeles", name="magical venue")
        ext.db.session.add(venue2)
        self.assertRaises(exc.IntegrityError, ext.db.session.commit)


    def test_create_event(self):
        name = "magical event"
        time = datetime.utcnow()
        event = models.Event(name=name, date=time)
        ext.db.session.add(event)
        ext.db.session.commit()

    def test_create_ticket(self):
        uid = str(uuid.uuid4())
        email = 'some@where.com'
        user = models.User(uid=uid, email=email)
        ext.db.session.add(user)

        venue = models.Venue(location="Los Angeles", name="magical venue")
        ext.db.session.add(venue)
 
        name = "magical event"
        time = datetime.utcnow()
        event = models.Event(name=name, date=time)
        ext.db.session.add(event)
        
        ext.db.session.commit()
        
        # now with all the pieces create ticket
        tid = str(uuid.uuid4())
        seat = 2
        isle = 4
        barcode = '\x00'
        ticket = models.Ticket(
            venue_id = venue.venue_id,
            event_id = event.event_id,
            user_id = user.user_id,
            tid = tid,
            seat = seat,
            isle = isle,
            barcode = barcode
        )
        ext.db.session.add(ticket)
        ext.db.session.commit()

        self.assertEqual(ticket.ticket_id, 1)


    def test_create_ticket_invalid_user(self):
        venue = models.Venue(location="Los Angeles", name="magical venue")
        ext.db.session.add(venue)
 
        name = "magical event"
        time = datetime.utcnow()
        event = models.Event(name=name, date=time)
        ext.db.session.add(event)
        
        ext.db.session.commit()
        
        # now with all the pieces create ticket
        tid = str(uuid.uuid4())
        seat = 2
        isle = 4
        barcode = '\x00'
        ticket = models.Ticket(
            venue_id = venue.venue_id,
            event_id = event.event_id,
            user_id = 5,
            tid = tid,
            seat = seat,
            isle = isle,
            barcode = barcode
        )
        ext.db.session.add(ticket)
        self.assertRaises(exc.IntegrityError, ext.db.session.commit)



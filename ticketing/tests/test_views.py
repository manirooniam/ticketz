# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)

import uuid
from datetime import datetime
import random
import string

from sqlalchemy import exc
from flask_sqlalchemy import SQLAlchemy as sqlalchemy
from flask_sqlalchemy import orm
from flask_testing import TestCase
from werkzeug import datastructures


from ticketing import app
from ticketing import ext
from ticketing import models

def generate_random(N = 5):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))


class TestModels(TestCase):
    def create_app(self):
        self.app = app.create_app()
        self.app.config['TESTING'] = True
        return self.app

    def create_flask_fixtures(self):
        self._flask_session_fixture = datastructures.CallbackDict()
        self._flask_session_fixture.sid = str(uuid.uuid4())

    def get_engine(self):
        with self.app.test_request_context():
            return ext.db.engine


    def setUp(self):
        super(TestModels, self).setUp()
        orm.configure_mappers()
        ext.db.create_all()
        self._flask_session_fixture = datastructures.CallbackDict()
        self._flask_session_fixture.sid = str(uuid.uuid4())

    def create_user(self):
        uid = str(uuid.uuid4())
        email = '%s@where.com' % generate_random()
        user = models.User(uid=uid, email=email)
        ext.db.session.add(user)
        ext.db.session.commit()
        return user

    def create_venue(self):
        venue = models.Venue(
            location="%s, Los Angeles" % generate_random(), 
            name="%s venue" % generate_random()
        )
        ext.db.session.add(venue)
        ext.db.session.commit()
        return venue

    def create_event(self):
        name = "%s magical event" % generate_random()
        time = datetime.utcnow()
        event = models.Event(name=name, date=time)
        ext.db.session.add(event)
        ext.db.session.commit()
        return event

    def tearDown(self):
        super(TestModels, self).tearDown()
        ext.db.session.remove()
        ext.db.drop_all()

    def test_index(self):
        response = self.client.post('/', data=None)
        self.assert200(response)

    def test_create_ticket(self):
        user = self.create_user()
        venue = self.create_venue()
        event = self.create_event()
        data = {
            'user': user.user_id,
            'venue': venue.venue_id,
            'event': event.event_id,
            'seat': 10,
            'isle': 5
        }

        response = self.client.post('/api/tickets', json=data)
        self.assertEqual(10, response.json['seat'])
        self.assertEqual(5, response.json['isle'])

        t = models.Ticket.query.filter_by(tid=response.json['ticket']).first()
        self.assertEqual(t.tid, response.json['ticket'])


    def test_create_ticket_missing_fields(self):
        response = self.client.post('/api/tickets', json={})
        self.assert403(response)


    def test_get_ticket(self):
        user = self.create_user()
        venue = self.create_venue()
        event = self.create_event()
        tid = str(uuid.uuid4())
        ticket = models.Ticket(
            user_id = user.user_id,
            venue_id = venue.venue_id,
            event_id = event.event_id,
            seat = 20,
            isle = 10,
            barcode = '\x00',
            tid = tid
        )
        ext.db.session.add(ticket)
        ext.db.session.commit()
        
        response = self.client.get('/api/tickets/%s' % ticket.tid)
        self.assert200(response)
        self.assertEqual(response.json['ticket'], ticket.tid)

    def test_get_ticket_doesnt_exist(self):
        response = self.client.get('/api/tickets/%s' % 'agc')
        self.assert404(response)



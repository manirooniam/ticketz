# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)

import os

import flask
import sqlalchemy

import ticketing.ext as ext
import ticketing.models
from ticketing import views


def create_app():
    app = flask.Flask('ticketing')

    # get the db url based off env
    logger.debug(os.environ['TICKETING_TESTING'])

    if os.environ['TICKETING_TESTING'] == 'True':
        db_url = os.environ['TICKETING_TEST_DB_URL']
    else:
        db_url = os.environ['TICKETING_DB_URL']    

    configure_db(app, db_url)

    # register views
    app.register_blueprint(views.ticket)

    return app

def configure_db(app, db_url):
    app.config['SQLALCHEMY_DATABASE_URI'] = db_url
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    logger.debug('connecting to db with url %s' % db_url)

    ext.db.init_app(app)


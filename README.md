# ticketz

### Welcome!

In order to be able to run the tests and/or server you first need to follow the instructions below

1. (Optional) Create virtualenv for python
2. pip install -r requirements
3. Install postgresql and create user/password for two different databases (the test one gets truncated)
4. update base.sh with the database information


#### Running unit & acceptance tests
The reason we create two databases is because the tests will drop all the tables and recreate them for each test.

To run the unit / acceptance tests simply do:
./testing.sh /path/to/nose ticketing/


#### Running the rest server
The rest server listens on port 8000 however, before attempting to start the server, please run the shovel tasks so that all the various tables are created for you.

To create the tables: ./dev /path/to/shovel db

You should see output from Postgres indicating the various tables that are being created

To run the rest server just do:
./dev /path/to/python rest_server.py




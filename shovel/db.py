# -*- coding: utf-8 -*-
import sys 
import os
sys.path.append('')
  
import shovel
  
from flask_sqlalchemy import sqlalchemy

from ticketing import app 
from ticketing import ext
from ticketing import models
  
@shovel.task
def create_tables():
    theapp = app.create_app()
    with theapp.test_request_context():
        ext.db.engine.echo = True
        sqlalchemy.orm.configure_mappers()
        ext.db.create_all()
  
  

